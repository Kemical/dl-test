import 'babel-polyfill';
import {
    Array1D, Array2D, ENV, Graph, Session, SGDOptimizer, CostReduction,
    InCPUMemoryShuffledInputProviderBuilder
} from 'deeplearn';

import gen from 'random-seed';

import {log, createForwardProp} from './src/dl';
import {train} from './src/train';

const rand = gen.create('my-seed');

const m = 100;
const learningRate = 0.003;
const epochs = 1000;
const hiddenLayersSizes = [12,12,28,28,28,28,12,12,24,24];

const testVal = [
    [81, 97],
    [2, 3]
];

const data = new Array(m).fill(1)
    .map(() => [rand.random(), rand.random()])
    .map(xs => xs.map(x => x * 100))
    //.map(([x1, x2]) => [Math.floor(x1), Math.floor(x2)])
    .map(([x1, x2]) => [x1 * x2, x1, x2]);

const inputs = data.map(([y, ...x]) => [...x]);
const labels = data.map(([y, ...rest]) => [y]);



console.log(data[0]);
console.log(data[1]);
console.log(data[2]);


document.getElementById('train-bt').addEventListener('click', async () =>{
    const predict = await train(inputs, labels, learningRate, hiddenLayersSizes, epochs);
    predict(testVal);
});

